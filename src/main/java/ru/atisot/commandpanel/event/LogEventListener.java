package ru.atisot.commandpanel.event;

import ru.atisot.commandpanel.util.Config;

import static java.lang.String.format;

public class LogEventListener implements EventListener {

    public LogEventListener() {
    }

    @Override
    public void update(String eventType, String message) {
        Config.getLogger().info(format("Command %s return message: %s", eventType, message));
    }
}
