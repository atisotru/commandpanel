package ru.atisot.commandpanel.event;

public interface EventListener {
    void update(String eventType, String message);
}
