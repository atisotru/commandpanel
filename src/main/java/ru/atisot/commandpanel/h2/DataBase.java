package ru.atisot.commandpanel.h2;

import ru.atisot.commandpanel.Handler;
import ru.atisot.commandpanel.server.Server;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase {

    private String path;
    private String user;
    private String password;
    private Connection connection;

    public DataBase(String path, String user, String password) {

        this.path = path;
        this.user = user;
        this.password = password;

        try {
            connection = DriverManager.getConnection("jdbc:h2:file:"+ this.path +";trace_level_file=0", this.user, this.password);
        } catch (SQLException e) {
            System.out.println("Database connection failure: "
                    + e.getMessage());
        }
    }

    public DataBase(String path) {

        this.path = path;

        try {
            connection = DriverManager.getConnection("jdbc:h2:file:"+ this.path +";trace_level_file=0");
        } catch (SQLException e) {
            System.out.println("Database connection failure: "
                    + e.getMessage());
        }
    }

    private void open() {

        try {

            if (connection.isClosed()) {
                connection = DriverManager
                        .getConnection("jdbc:h2:file:"+ this.path +";trace_level_file=0", this.user, this.password);
            }

        } catch (SQLException e) {
            System.out.println("Database connection failure: "
                    + e.getMessage());
        }
    }

    public void createTable() {

        open();

        final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS `servers` (`name` VARCHAR(32) NOT NULL, `ip` VARCHAR(16), `port` MEDIUMINT(5), `hash` VARCHAR(255), PRIMARY KEY (`name`))";

        try (Statement dataQuery = connection.createStatement()) {
            dataQuery.execute(CREATE_TABLE);
        } catch (SQLException e) {
            System.out.println("Database execution failure: "
                    + e.getMessage());
        }
    }

    public boolean addServer(Server server) {

        open();

        final String query = "INSERT INTO `servers` (`name`, `ip`, `port`, `hash`) values (?,?,?,?)";

        if (search(server.name()) == null && search(server.ip(), server.port()) == null) {
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, server.name());
                stmt.setString(2, server.ip());
                stmt.setInt(3, server.port());
                stmt.setString(4, Handler.crypt().encrypt(server.password(), Handler.auth().password()));
                stmt.execute();

                return true;

            } catch (SQLException e) {
                System.out.println("Database execution failure: "
                        + e.getMessage());
            }
        }

        return false;
    }

//    "DELETE FROM `servers` \n" +
//            "WHERE name = ? AND \n" +
//            " model NOT IN (SELECT model \n" +
//            " FROM PC\n" +
//            " );";

    public boolean removeServer(String name) {

        open();

        final String query = "DELETE FROM `servers` WHERE `name`=?";

        if (!search(name).name().isEmpty()) {
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, name);
                stmt.execute();

                return true;

            } catch (SQLException e) {
                System.out.println("Database execution failure: "
                        + e.getMessage());
            }
        }

        return false;
    }

    public boolean clear() {

        open();

        final String query = "DELETE FROM `servers`";

        try (Statement dataQuery = connection.createStatement()) {
            dataQuery.execute(query);
            return true;
        } catch (SQLException e) {
            System.out.println("Database execution failure: "
                    + e.getMessage());
        }
        return false;
    }

    public boolean updateServer(String name, Server server) {

        open();

        final String query = "UPDATE `servers` SET name=?, ip=?, port=?, hash=? WHERE `name`=?";

        Server s = search(name);

        if (s != null && s.name().equalsIgnoreCase(name)) {
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, server.name());
                stmt.setString(2, server.ip());
                stmt.setInt(3, server.port());
                stmt.setString(4, Handler.crypt().encrypt(server.password(), Handler.auth().password()));
                stmt.setString(5, name);
                stmt.execute();

                return true;

            } catch (SQLException e) {
                System.out.println("Database execution failure: "
                        + e.getMessage());
            }
        }

        return false;
    }

    public List<Server> servers() {

        open();

        List<Server> servers = new ArrayList<>();

        try (PreparedStatement query =
                     connection.prepareStatement("SELECT * FROM `servers`")) {
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                String ip = rs.getString("ip");
                int port = rs.getInt("port");
                String hash = rs.getString("hash");

                String password = Handler.crypt().decrypt(hash, Handler.auth().password());

                servers.add(new Server(name, ip, port, password));
            }
            rs.close();
        }
        catch (SQLException e) {
            System.out.println("Database execution failure: "
                + e.getMessage());
        }

        return servers;
    }

    public Server search(String name) {

        open();

        String query = "SELECT * FROM `servers` WHERE `name`=?";


        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                return new Server(
                        rs.getString("name"),
                        rs.getString("ip"),
                        rs.getInt("port"),
                        Handler.crypt().decrypt(rs.getString("hash"),
                                Handler.auth().password()));
            }
            rs.close();
        }
        catch (SQLException e) {
            System.out.println("Database execution failure: "
                    + e.getMessage());
        }

        return null;
    }

    public Server search(String ip, int port) {

        open();

        String query = "SELECT * FROM `servers` WHERE `ip`=? AND `port`=?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, ip);
            stmt.setInt(2, port);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                return new Server(
                        rs.getString("name"),
                        rs.getString("ip"),
                        rs.getInt("port"),
                        Handler.crypt().decrypt(rs.getString("hash"),
                                Handler.auth().password()));
            }
            rs.close();
        }
        catch (SQLException e) {
            System.out.println("Database execution failure: "
                    + e.getMessage());
        }

        return null;
    }
}
