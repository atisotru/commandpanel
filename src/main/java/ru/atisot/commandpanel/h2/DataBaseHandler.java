package ru.atisot.commandpanel.h2;

import ru.atisot.commandpanel.Handler;
import ru.atisot.commandpanel.util.Config;

import java.util.Scanner;

public class DataBaseHandler {

    private static Scanner in;

    private DataBase db;

    public DataBaseHandler() {

        in = new Scanner(System.in);

        String path = Config.dbPath();
        String user = Config.dbUser();
        String password = password();

        if (user.isEmpty()) {
            System.out.print("Set database user: "); // TODO
            String newuser = in.next();
            if (!newuser.isEmpty()) {
                user = newuser;
            } else System.out.printf("Set default database user: %s", user); // TODO

            Config.setDbUser(user);
        }

        if (password.isEmpty()) {
            System.out.print("Set database password: "); // TODO
            password = in.next();
            setPassword(password);
        }

        if (!password.isEmpty() && !user.isEmpty()) {
            db = new DataBase(path, user, password);
        } else
            db = new DataBase(path);

        db.createTable();
    }

    private void setPassword(String password) {
        Config.setDbPasswdHash(Handler.crypt().encrypt(password, Handler.auth().password()));
    }

    private String password() {
        String passHash = Config.dbPasswdHash();

        if(passHash.isEmpty()) return "";

        return Handler.crypt().decrypt(passHash, Handler.auth().password());
    }

    public DataBase dataBase() {
        return db;
    }
}
