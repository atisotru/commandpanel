package ru.atisot.commandpanel;

import io.graversen.minecraft.rcon.RconClient;
import ru.atisot.commandpanel.event.LogEventListener;
import ru.atisot.commandpanel.h2.DataBase;
import ru.atisot.commandpanel.module.commands.Command;
import ru.atisot.commandpanel.module.commands.server.Kick;
import ru.atisot.commandpanel.server.Server;
import ru.atisot.commandpanel.util.Config;
import ru.atisot.commandpanel.util.Rcon;

import java.util.concurrent.TimeUnit;

public class CommandPanel {


    public static void main(String[] args) {

        Config.getLogger().info("App. start...");

        DataBase db = Handler.dataBaseHandler().dataBase();

        //Server server = new Server("test1710", "127.0.0.1", 25575, "g87h57h");
        //db.addServer(server);
        //db.servers().forEach(a -> System.out.print(a.toString() + "\r\n"));
        //List<Server> servers = db.servers();

        Server server = db.search("test1710");

        System.out.println(server.toString());

        RconClient rcon = (new Rcon(server.ip(), (short) server.port(), server.password())).open();
        if (rcon != null) {
            Command command = new Kick(rcon, "player", "test");

            ((Kick) command).events.subscribe("kick", new LogEventListener());

            command.execute(1, TimeUnit.SECONDS);

        } else System.out.println("Error to connect!");

        //db.clear();

    }
}

//    private RconClient rcon;
//    try {
//        if(!rcon.ping())
//            rcon = RconClient.connect(ip + port, password);
//        rcon.close();
//    } catch (Exception e) {
//        Logger.getLogger().error(e.getMessage());
//    }

// Prepare the builders
//        final TellRawCommandBuilder tellRawCommandBuilder = new TellRawCommandBuilder();
//        final TitleCommandBuilder titleCommandBuilder = new TitleCommandBuilder();
//        final GiveCommandBuilder giveCommandBuilder = new GiveCommandBuilder();
//
//// Attempt to open and authenticate a RCON connection
//// We assume the server is running locally, and the RCON password is "abc123"
//// If no port is specified, it will use the default RCON port
//        final RconClient rconClient = RconClient.connect("localhost", "abc123");
//
//// Build a TellRaw command
//        final TellRawCommand tellRawCommand1 = tellRawCommandBuilder
//                .targeting(Selectors.ALL_PLAYERS)
//                .withText("It's dangerous to go alone - ")
//                .withColor(Colors.GRAY)
//                .italic()
//                .build();
//
//// Build another TellRaw command
//// The RCON client is able to transparently string together multiple TellRaw commands,
//// if for example different colors and formatting is desired for a single message
//        final TellRawCommand tellRawCommand2 = tellRawCommandBuilder
//                .targeting(Selectors.ALL_PLAYERS)
//                .withText("Take this!")
//                .withColor(Colors.DARK_AQUA)
//                .italic()
//                .build();
//
//// Let's also add a nice title to the players' screens
//        final TitleCommand titleCommand = titleCommandBuilder
//                .targeting(Selectors.ALL_PLAYERS)
//                .atPosition(TitlePositions.TITLE)
//                .withColor(Colors.GREEN)
//                .withText("Welcome!")
//                .build();
//
//// We'll give everyone a diamond sword - it's dangerous without
//        final GiveCommand giveCommand = giveCommandBuilder
//                .targeting(Selectors.ALL_PLAYERS)
//                .withItem("minecraft", "diamond_sword")
//                .amount(1)
//                .build();
//
//// Send the two TellRaw commands
//        rconClient.rcon().tellRaw(tellRawCommand1, tellRawCommand2);
//
//// Set the title
//        rconClient.rcon().title(titleCommand);
//
//// Grant the players their weapon
//        rconClient.rcon().give(giveCommand);
//
//// It is also easy to change game rules of the server
//        rconClient.rcon().gameRules().setGameRule(GameRules.MOB_GRIEFING, false);
//
//// Changing time of day is trivial as well
//        rconClient.rcon().time(TimeLabels.DAY);

