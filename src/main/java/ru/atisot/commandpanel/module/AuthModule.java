package ru.atisot.commandpanel.module;

import ru.atisot.commandpanel.Handler;
import ru.atisot.commandpanel.util.Config;

import java.util.Scanner;

public class AuthModule {

    static Scanner in;

    private String password;

    public AuthModule() {
        password = "";

        in = new Scanner(System.in);

        auth(); // Обязательно
    }

    private void auth() {
        int i = 0;
        int max = 3;
        while (!checkPassword()) {
            if (i < max) {

                if (i > 0) {
                    System.out.printf("You have %d more attempts!\n", max - i);
                }

                System.out.print("Enter or set application password: ");
                this.password = in.next();

                setPassword(this.password); // сохранит, если не задан
            } else {
                System.out.print("All input attempts have been exhausted. Try later.\n");
                System.exit(1);
            }
            i++;
        }
        System.out.print("You are logged in!\n");
    }

    public String password() {
        return this.password;
    }

    public void setPassword(String password) {

        if (Config.appPasswdHash().isEmpty()) {
            String hash = Handler.crypt().hashing(password);
            Config.setAppPasswdHash(hash);
            this.password = password;
            System.out.println("Password has been set successfully!"); // TODO
        }
    }

    public void setPassword(String oldPassword, String newPassword) {

        if (Config.appPasswdHash().isEmpty()) {
            String hash = Handler.crypt().hashing(newPassword);
            Config.setAppPasswdHash(hash);
            this.password = newPassword;
            System.out.println("Password has been set successfully!"); // TODO
        }
        else {
            String oldHash = Config.appPasswdHash();

            if (Handler.crypt().checkHash(oldPassword, oldHash)) {
                String hash = Handler.crypt().hashing(newPassword);
                Config.setAppPasswdHash(hash);
                this.password = newPassword;
                System.out.println("New password successfully set!"); // TODO
            } else {
                System.out.println("Wrong password!"); // TODO
                //System.exit(1);
            }
        }
    }

    private boolean checkPassword() {

        if (Config.appPasswdHash().isEmpty()) return false;

        boolean check = Handler.crypt().checkHash(this.password, Config.appPasswdHash());

        if (!this.password.isEmpty() && !check) System.out.println("Wrong password!"); // TODO

        return check;
    }

    public boolean isAuth() {
        return !this.password.isEmpty() && checkPassword();
    }
}
