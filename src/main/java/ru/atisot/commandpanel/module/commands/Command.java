package ru.atisot.commandpanel.module.commands;

import java.util.concurrent.TimeUnit;

public abstract class Command {

    abstract public void execute(long timeout, TimeUnit timeunit);
}
