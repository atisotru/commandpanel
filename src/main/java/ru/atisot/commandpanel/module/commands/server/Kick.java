package ru.atisot.commandpanel.module.commands.server;

import io.graversen.minecraft.rcon.RconClient;
import io.graversen.minecraft.rcon.RconResponse;
import ru.atisot.commandpanel.event.EventManager;
import ru.atisot.commandpanel.module.commands.Command;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Kick extends Command {

    public RconClient rcon;

    private String player;
    private String reason;

    public EventManager events;

    public Kick(RconClient rcon, String player, String reason) {
        this.rcon = rcon;
        this.player = player;
        this.reason = reason;
        this.events = new EventManager("kick");
    }

    @Override
    public void execute(long timeout, TimeUnit timeunit) {
        RconResponse res = null;
        String command = String.join(" ", "kick", player, reason);

        try {
            res = rcon.sendRaw(command).get(timeout, timeunit);

            events.notify("kick", res.getResponseString());

            rcon.close();
        } catch (IOException | InterruptedException | ExecutionException | TimeoutException e) { // TODO
            e.printStackTrace();
        }
    }
}
