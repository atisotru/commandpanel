package ru.atisot.commandpanel.server;

public class Server {

    private String ip;
    private int port;
    private String name;

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void name(String password) {
        this.password = password;
    }

    private String password;

    public Server(String name, String ip, int port, String password) {
        this.ip = ip;
        this.port = port;
        this.name = name;
        this.password = password;
    }

    public String ip() {
        return ip;
    }

    public int port() {
        return port;
    }

    public String name() {
        return name;
    }

    public String password() {
        return password;
    }


    @Override
    public String toString() {
        return String.format("%s:%s:%d", name, ip, port);
    }
}