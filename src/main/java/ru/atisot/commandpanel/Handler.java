package ru.atisot.commandpanel;

import ru.atisot.commandpanel.h2.DataBaseHandler;
import ru.atisot.commandpanel.module.AuthModule;
import ru.atisot.commandpanel.util.Crypt;

public class Handler {

    private static Crypt crypt;
    private static AuthModule auth;
    private static DataBaseHandler dataBaseHandler;

    static {
        // ОБЯЗАТЕЛЬНЫЕ МОДУЛИ
        crypt = new Crypt();
        auth = new AuthModule();
        dataBaseHandler = new DataBaseHandler();
    }

    private Handler() {
    }

    public static Crypt crypt() {
        return crypt;
    }
    public static DataBaseHandler dataBaseHandler() {
        return dataBaseHandler;
    }
    public static AuthModule auth() {
        return auth;
    }
}