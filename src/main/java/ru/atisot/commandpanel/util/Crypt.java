package ru.atisot.commandpanel.util;

import com.google.crypto.tink.*;
import com.google.crypto.tink.aead.AeadConfig;
import com.google.crypto.tink.aead.AeadFactory;
import com.google.crypto.tink.aead.AeadKeyTemplates;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Base64;

public class Crypt {

    private Aead aead;
    private KeysetHandle keysetHandle;

    public Crypt() {
        try {
            keysetHandle = getKeyHandle();

            aead = AeadFactory.getPrimitive(keysetHandle);

        } catch (GeneralSecurityException e) {
            e.printStackTrace(); // TODO
            System.exit(1);
        }
    }

    public String encrypt(String text, String key) {
        String result = "";

        try {
            result = Base64.getEncoder().encodeToString(aead.encrypt(text.getBytes(), key.getBytes()));

        } catch (GeneralSecurityException e) {
            e.printStackTrace(); // TODO
        }

        return result;
    }

    public String decrypt(String ciphertext, String key) {

        String result = "";

        try {
            result = new String(aead.decrypt(Base64.getDecoder().decode(ciphertext), key.getBytes()));

        } catch (GeneralSecurityException e) {
            e.printStackTrace(); // TODO
        }

        return result;
    }

    public String hashing(String text) {

        return BCrypt.hashpw(text, BCrypt.gensalt(12));
    }

    public boolean checkHash(String text, String hash) {

        return BCrypt.checkpw(text, hash);
    }

    private KeysetHandle getKeyHandle() {

        KeysetHandle keysetHandle = null;

        try {
            AeadConfig.register();

            String str = new String(Files.readAllBytes(Paths.get(Config.keysetPath())), StandardCharsets.UTF_8);

            JSONObject json = null;

            if (!str.isEmpty()) {
                json = new JSONObject(str);
            }

            if (json != null) {
                keysetHandle = CleartextKeysetHandle.read(JsonKeysetReader.withFile(new File(Config.keysetPath())));
            } else {
                keysetHandle = KeysetHandle.generateNew(AeadKeyTemplates.AES128_GCM);

                CleartextKeysetHandle.write(keysetHandle, JsonKeysetWriter.withFile(new File(Config.keysetPath())));
            }
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return keysetHandle;
    }
}
