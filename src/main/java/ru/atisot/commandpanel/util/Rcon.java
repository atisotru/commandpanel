package ru.atisot.commandpanel.util;

import io.graversen.minecraft.rcon.RconClient;

public class Rcon{

    private RconClient rcon = null;
    private String ip;
    private short port;
    private String password;

    public Rcon(String ip, short port, String password) {
        this.ip = ip;
        this.port = port;
        this.password = password;
    }

    public RconClient open() {
        try {
            //if(!rcon.ping()) {
            this.rcon = RconClient.connect(this.ip, this.password, this.port);
            //}
        } catch (Exception e) {
            Config.getLogger().error(e.getMessage());
        }

        return this.rcon;
    }
}
