package ru.atisot.commandpanel.util;

import org.apache.log4j.Logger;
import ru.atisot.commandpanel.Handler;

import java.io.*;
import java.util.Properties;

public class Config {

    // Инициализация логера
    private static final Logger log = Logger.getLogger(Config.class);

    private static Properties props;

    private static String configPath;
    private static String dbPath;

    private static final String dbUserParam = "DATABASE_USER";
    private static final String dbPasswdHashParam = "DATABASE_PASSWD_HASH";
    private static final String appPasswdHashParam = "APP_PASSWD_HASH";
    private static final String keysetPathParam = "KEYSET_PATH";

    static {
        configPath = getFilePath("CommandPanel/config.properties", true);
        dbPath = getFilePath("CommandPanel/base", false);

        props = new Properties();
        if (props.isEmpty()) load();
    }

    private Config() {}

    public static String configPath() {
        return configPath;
    }

    public static String dbPath() {
        return dbPath;
    }

    public static String dbUser() {
        String user = String.valueOf(props.getProperty(dbUserParam, ""));

        if (user.isEmpty()) return "";

        return Handler.crypt().decrypt(user, Handler.auth().password());
    }

    public static void setDbUser(String user) {
        props.setProperty(dbUserParam, Handler.crypt().encrypt(user, Handler.auth().password()));
        Config.save();
    }

    public static String dbPasswdHash() {
        return String.valueOf(props.getProperty(dbPasswdHashParam, ""));
    }

    public static void setDbPasswdHash(String dbPasswdHash) {
        props.setProperty(dbPasswdHashParam, dbPasswdHash);
        Config.save();
    }

    public static String appPasswdHash() {
        return String.valueOf(props.getProperty(appPasswdHashParam, ""));
    }

    public static void setAppPasswdHash(String appPasswdHash) {
        props.setProperty(appPasswdHashParam, appPasswdHash);
        Config.save();
    }

    public static Logger getLogger() {
        return log;
    }

    public static void setKeysetPath(String path) {
        props.setProperty(keysetPathParam, path);
        Config.save();
    }

    public static String keysetPath() {
        String keysetPath = String.valueOf(props.getProperty(keysetPathParam, "CommandPanel/keyset.json"));
        return getFilePath(keysetPath, true);
    }

    // сохранение конфигурации
    private static void save(){
        try {
            props.store(new FileOutputStream(configPath), null);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
    }

    // загрузка конфигурации
    private static void load(){
        try {
            props.load(new FileInputStream(new File(configPath)));
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
    }

    // перезагрузка конфигурации
    public static void reload() {
        load();
        log.info("Configuration reloaded.");
    }

    private static String getFilePath(String filePath, boolean create) {
        File file = new File(filePath);

        try {
            if(create && !file.exists()) {
                PrintWriter writer = new PrintWriter(filePath, "UTF-8");
                writer.close();
            }
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }

        return file.getAbsolutePath();
    }
}
