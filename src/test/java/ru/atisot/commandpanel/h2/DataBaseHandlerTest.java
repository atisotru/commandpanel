package ru.atisot.commandpanel.h2;

import org.junit.After;
import org.junit.Test;
import ru.atisot.commandpanel.Handler;
import ru.atisot.commandpanel.server.Server;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class DataBaseHandlerTest {

    private final InputStream systemIn = System.in;

    private ByteArrayInputStream testIn;

    final String password = "password";

    private DataBase db;

    public DataBaseHandlerTest() {
        provideInput(password);
        db = Handler.dataBaseHandler().dataBase();
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void restoreSystemInput() {
        System.setIn(systemIn);
    }

    @Test
    public void dataBase() {

        //db.createTable();

        Server server = new Server("server3", "127.0.0.1", 25567, "testpass222");
        Server server1 = new Server("server 4", "127.0.0.1", 25568, "tfgfj77j");

        db.addServer(server);
        db.addServer(server1);

        db.servers().stream().forEach(a -> System.out.printf(a.toString() + "\r\n"));
    }
}