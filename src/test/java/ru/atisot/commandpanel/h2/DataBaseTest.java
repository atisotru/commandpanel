// https://javarush.ru/groups/posts/605-junit

package ru.atisot.commandpanel.h2;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.atisot.commandpanel.Handler;
import ru.atisot.commandpanel.server.Server;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DataBaseTest {

    private final InputStream systemIn = System.in;

    private ByteArrayInputStream testIn;

    final String password = "password";

    private DataBase db;

    public DataBaseTest() {
        provideInput(password);
        db = Handler.dataBaseHandler().dataBase();
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void restoreSystemInput() {
        System.setIn(systemIn);
    }

    @Test
    public void addServer() {

        db.clear();

        Server server = new Server("addtest", "127.0.0.1", 25565, "df67et7eh5");

        Assert.assertTrue(db.addServer(server));
    }

    @Test
    public void removeServer() {

        db.clear();

        Server server = new Server("removetest", "127.0.0.2", 25566, "dfgjt7k96sj");

        Assert.assertTrue(db.addServer(server));
        Assert.assertTrue(db.removeServer(server.name()));
    }

    @Test
    public void updateServer() {

        db.clear();

        Server server = new Server("updatetest", "127.0.0.3", 25565, "dfgjt7k96sj");

        Assert.assertTrue(db.addServer(server)); // добавим тестовый сервер

        String oldName = server.name();

        server.setName("updatetest2");
        Assert.assertTrue(db.updateServer(oldName, server));

        Assert.assertEquals(server.name(), db.search(server.name()).name()); // проверим обновились ли данные
    }

    @Test
    public void servers() {

        db.clear();

        Server server = new Server("test1", "127.0.0.1", 25565, "sd75g5gh56");
        Server server1 = new Server("test2", "127.0.0.2", 25565, "d5gh45684hg");
        Server server2 = new Server("test3", "127.0.0.3", 25565, "sso5udhh56");

        Assert.assertTrue(db.addServer(server));
        Assert.assertTrue(db.addServer(server1));
        Assert.assertTrue(db.addServer(server2));

        List<Server> list = new ArrayList<>();
        list.add(server);
        list.add(server1);
        list.add(server2);

        List<Server> list1 = db.servers();

        Assert.assertEquals(list.toString(), list1.toString());
    }

    @Test
    public void search() {

        db.clear();

        Server server = new Server("test1", "127.0.0.1", 25565, "sd75g5gh56");
        Assert.assertTrue(db.addServer(server));

        Assert.assertEquals(server.name(), db.search(server.name()).name());
    }

    @Test
    public void search1() {

        db.clear();

        Server server = new Server("test1", "127.0.0.1", 25565, "sd75g5gh56");
        Assert.assertTrue(db.addServer(server));

        Server server1 = db.search(server.ip(), server.port());
        Assert.assertEquals(server.name(), server1.name());
    }
}