package ru.atisot.commandpanel.util;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class ConfigTest {

    private final InputStream systemIn = System.in;

    private ByteArrayInputStream testIn;

    final String password = "password";

    public ConfigTest() {
        provideInput(password);
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void restoreSystemInput() {
        System.setIn(systemIn);
    }

    @Test
    public void configPath() {
        String path = Config.configPath();
        Assert.assertTrue(path, true);
    }

    @Test
    public void dbPath() {
        System.out.printf(String.valueOf(Config.dbPath()));
    }

    @Test
    public void dbUser() {
        System.out.printf(String.valueOf(Config.dbUser()));
    }

    @Test
    public void setDbUser() {
        String user1 = "user";
        Config.setDbUser(user1);

        String user2 = String.valueOf(Config.dbUser());

        Assert.assertEquals(user1, user2);
    }

    @Test
    public void reload() {
        Config.reload();
    }
}