package ru.atisot.commandpanel.util;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.atisot.commandpanel.Handler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class CryptTest {

    private final InputStream systemIn = System.in;

    private ByteArrayInputStream testIn;

    final String password = "password";

    private Crypt crypt;

    public CryptTest() {
        provideInput(password);
        crypt = Handler.crypt();
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void restoreSystemInput() {
        System.setIn(systemIn);
    }

    @Test
    public void encrypt() {
        String text = "текст";
        String key = "qwerty12345";

        String ciphertext = crypt.encrypt(text, key);

        System.out.print(ciphertext);
    }

    @Test
    public void decrypt() {

        String text = "текст";
        String key = "password";

        String ciphertext = crypt.encrypt(text, key);

        String dectext = crypt.decrypt(ciphertext, key);

        Assert.assertEquals(text, dectext);
    }

    @Test
    public void checkHash() {

        String text = "текст";

        String hash = crypt.hashing(text);

        Assert.assertTrue(crypt.checkHash(text, hash));
    }
}