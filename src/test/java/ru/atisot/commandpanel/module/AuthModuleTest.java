package ru.atisot.commandpanel.module;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.atisot.commandpanel.Handler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class AuthModuleTest {

    private final InputStream systemIn = System.in;

    private ByteArrayInputStream testIn;

    final String password = "password";

    public AuthModuleTest() {
        provideInput(password);
    }

    private void provideInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    @After
    public void restoreSystemInput() {
        System.setIn(systemIn);
    }

    @Test
    public void setPassword() {

        Handler.auth().setPassword(password);

        Assert.assertTrue(Handler.auth().isAuth());
    }

    @Test
    public void isAuth() {
    }
}